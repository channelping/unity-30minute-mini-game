﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace cjc
{

    public class Lively : MonoBehaviour
    {

        public bool stop = false;

        private void Start()
        { 
            InvokeRepeating("Move",0f, 1f);

        }

        float dir = 1;
        private void Move()
        {
            dir *= -1;
            transform.DOMoveX(dir * 1.618f, 0.98f).SetEase(Ease.InOutSine);

        }


        private void Update()
        {
            
            if( stop)
            {

                //取消所有定时回调
                CancelInvoke();

                //禁用当前脚本
                this.enabled = false;

                //停止缓动
                transform.DOPause();

            }

        }

    }

}