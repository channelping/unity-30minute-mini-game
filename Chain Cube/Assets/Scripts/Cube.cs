﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cjc
{

    public class Cube : MonoBehaviour
    {
        public int index = 1;
        public CubeLaunch mgr = null;

        private Lively livelyScript = null;
        private bool launched = false;
        private Rigidbody rig = null;



        private void Start()
        {
            livelyScript = GetComponent<Lively>();
            rig = GetComponent<Rigidbody>();

        }

        //拖拽
        private Vector3 screenPoint;
        private Vector3 offset;
        void OnMouseDown()
        {
            if (launched) return;

            if (null != livelyScript)
                livelyScript.stop = true;

            //得到cube 相对屏幕的位置
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

            //得到相对偏移
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }

        void OnMouseDrag()
        {
            if (launched) return;




            //获取当前屏幕坐标
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            //仅移动x轴
            var src = transform.position;
            src.x = (Camera.main.ScreenToWorldPoint(curScreenPoint) + offset).x;
            transform.position = src;
        }

        void OnMouseUp()
        {
            if (launched) return;
            launched = true;

            //发射
            DoLaunch();
        }


        void DoLaunch()
        {

            //	rig.velocity = Vector3.forward * 10;
            mgr.aim.target = null;

        }

        private void Update()
        {

            if (launched)
            {
                transform.Translate(Vector3.forward * 20 * Time.deltaTime);
            }
        }


        public void Upgrade()
        {

            if (null == mgr) return;
            ++index;
            CubeLaunch.maxIndex = Mathf.Max(CubeLaunch.maxIndex, index);
            this.tag = "preCube";

            //设置纹理
            var render = GetComponent<Renderer>();
            render.material.mainTexture = mgr.textures[index];
            //弹起
            rig.AddForce(Vector3.up * 6.18f
                + Vector3.right * Random.Range(-2, 2)
                + Vector3.forward * Random.Range(-0.618f, 2)
                , ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision collision)
        {

            var tag = collision.gameObject.tag;
            if ("fixedCube" == tag || "end" == tag)
            {
                this.enabled = false;
                this.tag = "fixedCube";

                //var cube = collision.gameObject.GetComponent<Cube>();
                //撞击到cube
                Cube cube = null;
                collision.gameObject.TryGetComponent<Cube>(out cube);
                if (null != cube && cube.index == this.index)
                {
                    Destroy(this.gameObject);
                    cube.Upgrade();
                }

                mgr.Spawn();
            }
        }
    }

}