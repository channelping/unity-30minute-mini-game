﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cjc
{

    public class AimingLine : MonoBehaviour
    {

        public Transform target = null;



        private void Update()
        {
            
            if( null != target)
            {
                // target 
                var src = transform.position;
                src.x = target.position.x;
                transform.position = src;

            }
            else
            {
                if (gameObject.activeSelf)
                    gameObject.SetActive(false);
            }


        }





    }

}