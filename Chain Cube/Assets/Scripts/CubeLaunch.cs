﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cjc
{

    public class CubeLaunch : MonoBehaviour
    {
        public GameObject cubePrefab;
        public Texture2D[] textures;
        public AimingLine aim;


        public Cube[] InitCubes;
        public static int maxIndex = 0;

        private void Start()
        {
            
            //初始化出现的待撞击box
            foreach( var cube in InitCubes)
            {
                var index = Random.Range(0, 2);
                //设置纹理
                var render = cube.GetComponent<Renderer>();
                render.material.mainTexture = textures[index];

                //设置对应下标
                var cubeScript = cube.GetComponent<Cube>();
                cubeScript.index = index;
                cubeScript.mgr = this;
            }

        }

        public void Spawn()
        {
            //存在未发射的cube
            if (null != aim.target) return;

            var instance = Instantiate(cubePrefab, transform);
            var index = Random.Range(0, 3);

            //设置纹理
            var render = instance.GetComponent<Renderer>();
            render.material.mainTexture = textures[index];

            //设置对应下标
            var cubeScript = instance.GetComponent<Cube>();
            cubeScript.index = index;
            cubeScript.mgr = this;

            //移除标准线的跟随坐标
            aim.target = instance.transform;
            aim.gameObject.SetActive(true);

            //缓动放大
            StartCoroutine(DoZoom(instance.transform));
        }


        IEnumerator DoZoom(Transform target)
        {
            float zoom = 0.01f;
            while (zoom < 1.0f)
            {
                target.localScale = new Vector3(zoom, zoom, zoom);
                zoom += Time.deltaTime * Mathf.PI;
                yield return 0;
            }
        }

    }

}