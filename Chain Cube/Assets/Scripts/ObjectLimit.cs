﻿using UnityEngine;
using System.Collections;

public class ObjectLimit : MonoBehaviour
{

    public float minX = -2.435f;
    public float maxX = 2.435f;
	public float minY = 0;
	public float maxY = 4;
	public float minZ = -0.15f;
	public float maxZ = 8.32f;

	void Update()
	{
		transform.localPosition = new Vector3(Mathf.Clamp(gameObject.transform.localPosition.x, minX, maxX),
										 	  Mathf.Clamp(gameObject.transform.localPosition.y, minY, maxY),
										 	  Mathf.Clamp(gameObject.transform.localPosition.z, minZ, maxZ));

	}

}