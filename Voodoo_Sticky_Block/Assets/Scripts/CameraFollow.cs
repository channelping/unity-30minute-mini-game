﻿
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] PlayerManager playerManager;
    public Transform target;

    [SerializeField] float smoothSpeed;
    [SerializeField] Vector3 offset;


    public Vector3 initPos;
    public void Start()
    {
        initPos = transform.position;
    }

    public static CameraFollow instance = null;
    private void Awake()
    {
        instance = this;
    }

    public void Renew( Transform target)
    {
        transform.position = initPos;
        this.target = target;
    }

    void LateUpdate()
    {
        if (target != null && playerManager.levelState== PlayerManager.LevelState.NotFinished) {
            Vector3 desiredPos= target.position+ offset;
            Vector3 smoothedPos = Vector3.Lerp (transform.position, desiredPos, smoothSpeed);
            transform.position = new Vector3(transform.position.x, transform.position.y, smoothedPos.z);
        }
    }
}
