﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : IGameElement
{

    public Material collectedObjMat;
    public PlayerState playerState;
    public LevelState levelState;

    public Transform partcilePrefab;

    public UIControl uIControl;
    public List<GameObject> collidedList;

    public Transform collectedPoolTransform;
    public enum PlayerState
    {
        Stop,
        Move
    }
    public enum LevelState 
    {
        NotFinished,
        Finished
    }

    public void CallMakeSphere () {
        foreach (GameObject obj in collidedList) {
            obj.GetComponent<CollectedObjController>().MakeSphere();
        }
        uIControl.ShowEnd(collidedList.Count);
    }

    public static PlayerManager instance = null;
    public void Awake()
    {
        instance = this;
    }

    public override void Init()
    {
        collectedPoolTransform = UIControl.lastPoolTrans;
    }
}
