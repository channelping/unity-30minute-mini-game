﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IGameElement : MonoBehaviour
{
   abstract public void Init();
}
