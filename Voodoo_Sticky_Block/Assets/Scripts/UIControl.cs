﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class UIControl : MonoBehaviour
{

    public GameObject endpage = null;
    public GameObject startpage = null;
    public Text endScoreLabel = null;
    public GameObject collectedPoolPref = null;
    public Transform stage = null;


    [HideInInspector]
    public GameObject lastPool = null;
    public static Transform lastPoolTrans = null;

    public void StartGame()
    {
        if (null != lastPool)
            GameObject.DestroyImmediate(lastPool);


        var t = Instantiate(collectedPoolPref, stage);
        var cf = GameObject.FindObjectOfType<CameraFollow>();
        cf.target = t.transform;
        lastPoolTrans = t.transform;
        lastPool = t;
        CameraFollow.instance.Renew(t.transform);

        startpage.SetActive(false);

        var gameElements = GameObject.FindObjectsOfType<IGameElement>();
        foreach (var el in gameElements)
        {
            el.Init();
        }
    }


    public void ShowEnd(int c)
    {


        DOTween.Sequence().AppendInterval(2f).AppendCallback(() =>
        {

            var cg = endpage.GetComponent<CanvasGroup>();
            cg.alpha = 1.0f;
            cg.DOFade(0f, 3.0f).OnComplete(() =>
            {
                endpage.SetActive(false);

            });

            startpage.SetActive(true);
            var cg2 = startpage.GetComponent<CanvasGroup>();
            cg2.alpha = 0f;
            cg2.DOFade(1.0f, 3.0f);
        });


        endpage.SetActive(true);
        endScoreLabel.text = $"X{c}";


    }
}
