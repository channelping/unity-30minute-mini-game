﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement :  IGameElement
{
    [SerializeField] PlayerManager playerManager;
    [SerializeField] float movementSpeed;
    [SerializeField] float controlSpeed;

    //Touch Settings
    [SerializeField] bool isTouching;
    float touchPosX;
    Vector3 direction;


    void Update()
    {
        GetInput();
    }

    public override void Init()
    {
        playerManager = PlayerManager.instance;
    }

    private void FixedUpdate() {
        
        if(playerManager.playerState==PlayerManager.PlayerState.Move) {
            transform.position += Vector3.forward * movementSpeed * Time.fixedDeltaTime;
        }
        if(isTouching) {
            touchPosX += Input.GetAxis("Mouse X") * controlSpeed * Time.fixedDeltaTime;
        }

        transform.position = new Vector3(touchPosX, transform.position.y, transform.position.z);
    }

    private void LateUpdate()
    {
        if(transform.position.z > 64)
        {
            GameObject.Destroy(gameObject, 1);
        }
    }

    void GetInput() {
        if(Input.GetMouseButton(0)) {
            isTouching=true;
        }
        else {
            isTouching=false;
        }
    }

   
}
